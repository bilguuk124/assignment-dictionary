%include "words.inc"
%include "lib.inc"
%define BUFFER 256
%define DQ_SIZE 8

extern find_word

global _start

section .data

not_found: db "Ключ не найден!", 0
overflow: db "Буфер переполнено", 0
string_buffer: times BUFFER db 0

section .text

; Начнём с ввода строки в буфер
_start:
    xor rax, rax
    mov rdi, string_buffer
    mov rsi, BUFFER
    call read_word
    test rax, rax
    jne .success_read_buffer    ; Если случается переполнение, то rax вернет 0 
    mov rdi, overflow
    call print_err
    call print_newline
    call exit

; 
.success_read_buffer:
    mov rdi, rax
    mov rsi, first
    push rdx                    ; Сохраняем длину строки ввода
    call find_word              
    test rax, rax               ; Если ключ не был найден то rax = 0
    jne .success_key_found
    mov rdi, not_found          
    call print_err
    call print_newline
    call exit

.success_key_found:
    pop rdx                     ; Вернем длину строки
    add rax, 8                  ; Указать в след. элемент
    add rax, rdx                ; добавим длину строки
    add rax, 1                  ; добавим 1 для нуль-термирование
    mov rdi, rax                ; Сейчас rax указывает на найденную строку
    call print_string
    call print_newline
    call exit

print_err:
    xor rax, rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall 
    ret
