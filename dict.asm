%define DQ 8
section .text
global find_word

extern string_length
extern string_equals

find_word:
    xor rax, rax
    mov r8, rdi
    mov r9, rsi
    .loop:
        add r9, DQ
        mov rsi, r9
        mov rdi, r8
        push r8
        push r9
        call string_equals
        pop r9
        pop r8
        cmp rax, 1
        je .found
        mov r9, [r9 - DQ]
        cmp r9, 0
        je .fail
        jmp .loop
    .found:
        sub r9, DQ
        mov rax, r9
        ret
    .fail:
        xor rax,rax
        ret
